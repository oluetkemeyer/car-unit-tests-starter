package org.launchcode.training;


import org.junit.Test;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class CarTest {

    //TODO: constructor sets gasTankLevel properly

    @Test
    public void testGasTankOnInit() {
        Car car = new Car("Ford", "Mustang", 10, 20);
        assertEquals(10, car.getGasTankLevel(), .001);
    }

    //TODO: gasTankLevel is accurate after driving within tank range

    @Test
    public void testAccurateGasTankLevelAfterDriving(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(20);
        assertEquals(9, testCar.getGasTankLevel(), .001);
    }

    //TODO: gasTankLevel is accurate after attempting to drive past tank range

    @Test
    public void testPastTankRange() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(250);
        assertEquals(0, testCar.getGasTankLevel(), .001);
    }

    //TODO: can't have more gas than tank size, expect an exception

    @Test(expected = IllegalArgumentException.class)
    public void testTankMax() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.addGas(5);
        fail("shouldn't get here");
    }

    @Test
    public void testConstructorRange(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        assertEquals(200, testCar.getRange(), .001);
    }

    @Test
    public void testAfterDriveRange(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(20);
        assertEquals(180, testCar.getRange(), .001);
    }

    @Test
    public void testAfterAddGasRange(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.setGasTankLevel(0);
        testCar.addGas(5);
        assertEquals(100, testCar.getRange(), .001);
    }

    @Test
    public void testTripMeterConstructor(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        assertEquals(0, testCar.getTripMeter(), .001);
    }

    @Test
    public void testTripMeterReset(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(20);
        testCar.resetTripMeter();
        assertEquals(0, testCar.getTripMeter(), .001);
    }

    @Test
    public void testTripMeterOperation(){
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(20);
        assertEquals(20, testCar.getTripMeter(), .001);
    }
}
